/*
 * copyright (C) 2001 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __PLUGIN_H__
#define __PLUGIN_H__

#include "feedback.h"


typedef struct PluginInfo
{
	GModule *module;
	gint (*start_plugin) (PluginInfo *, AppDetails *);
	gint (*stop_plugin) (PluginInfo *, AppDetails *);
	gint (*close_plugin) (PluginInfo *, AppDetails *);
	gchar *plugin_name;
	gchar *desc;
	gchar *author;
	gboolean running;
};

enum {
	PLUGIN_OK,
	PLUGIN_ERROR
};
/* plugins must have these functions */
gint start_plugin (PluginInfo *, AppDetails*);
gint stop_plugin (PluginInfo *, AppDetails*);
gint close_plugin (PluginInfo *, AppDetails*);
#endif /* __PLUGIN_H__ */
