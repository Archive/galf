/* 
 * copyright (C) 2001 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <glib.h>

  GdkDisplay *dpy;
  Display *x_dpy;
  GdkScreen *default_screen;
  GdkWindow* root_window;
  Window xroot;
  Atom client_list;

gboolean init ()
{
  GtkWidget *toplevel, *label;
  toplevel = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (toplevel, "destroy", G_CALLBACK (gtk_main_quit), NULL);
  label = gtk_label_new ("\nTimout app\n");
  gtk_container_add (GTK_CONTAINER (toplevel), label);
  gtk_widget_show_all (toplevel);
  return FALSE;
}


int main(int argc, char** argv)
{
  GdkCursor*  toto;
  GtkWidget* top_level, *button;
  Atom proxy_atom = NULL;
  int timeout = 10;
  
  
  gtk_init (&argc, &argv);

  if (argc == 2)
    timeout = atoi (argv[1]);

  g_print ("timeout is %d\n", timeout);

  g_timeout_add (timeout * 1000, (GSourceFunc) init, NULL);

  gtk_main ();
  
}	
