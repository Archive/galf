/*
 * copyright (C) 2001 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __FEEDBACK_H__
#define __FEEDBACK_H__
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <popt.h>
#include <libintl.h>

#define _(x) dgettext (GETTEXT_PACKAGE, x)

#define GALF_DEBUG 0


#define GALF_FEEDBACK_PROP_TEMPLATE "GALF_FEEDBACK_PROP_"
#define FEEDBACK_PROXY_WIN "GALF_PROXY_WIN"
#define DEFAULT_TIMEOUT 25
#define DEFAULT_FEEDBACK_TYPE "cursor"
#define GCONF_PREFS_DIR "/apps/galf"
#define WIN_LIST_ATOM "_NET_CLIENT_LIST"

typedef struct {
  GtkWidget *fake_task_win;
  Window  pointer_window;
  gboolean stopped;
} Feedback;

typedef struct {
  gchar	    *app_name;
  gchar	    *feedback_type;
  gchar	    *command_line;
  guint	     timeout;
  gint	     screen_num;
  gint	     pid;
  gulong     window_opened_handler;
  Feedback  *feedback;
} AppDetails;

typedef struct {
  gchar	    *plugin_name;
  gint	     default_timeout;
  gboolean   warn_if_process_died;
} Prefs;



void     free_app_details (AppDetails *d);
Window   check_server_running (GdkDisplay *dpy, Window *root);

gboolean get_feedback_request_details (Display *x_dpy, 
				       Window  win,
				       Atom request_atom, 
				       AppDetails *details);

gboolean set_feedback_request_details (Display *x_dpy,
				       Window  win,
				       AppDetails *details);

gboolean start_with_feedback	      (GdkDisplay *dpy, 
				       AppDetails *details);

void galf_verbose (const char *format, ...);
#endif /* __FEEDBACK_H */
