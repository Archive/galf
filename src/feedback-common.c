
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <glib.h>
#include <gconf/gconf.h>
#include <popt.h>
#include "feedback.h"

void
galf_verbose (const char *format, ...)
{
  va_list args;
  gchar *str;

  if (format == NULL)
    return;

  if (!GALF_DEBUG)
    return;
  
  va_start (args, format);
  str = g_strdup_vprintf (format, args);
  va_end (args);
  
  fputs (str, stderr);

  fflush (stderr);
  
  g_free (str);
}


void 
free_app_details (AppDetails *d)
{
  if (d->feedback)
    g_free (d->feedback);
  if (d->app_name)
    g_free (d->app_name);
  if (d->feedback_type)
    g_free (d->feedback_type);
  if (d->command_line)
    g_free (d->command_line);
  if (d)
    g_free (d);
}

  


/* check is the proxy_prop exist if so check if the window exists
 * return FALSE and xroot if the proxy_prop was previously set
 * else return proxy winid and xroot win */

Window
check_server_running (GdkDisplay *dpy, Window *root)
{
    Window* proxy_win = NULL, *check_win = NULL;
    Atom proxy_atom = 0;
    XWindowAttributes win_attrib;
    Display *x_dpy;
    int num_screens, i = 0;
    gboolean proxy_prop_exist = FALSE;
    
    Atom actual_type;
    int actual_format;
    unsigned long n_items_return, bytes_after_return;
    gboolean result;
    
    
    x_dpy = gdk_x11_display_get_xdisplay (dpy);

    num_screens = gdk_display_get_n_screens (dpy);

    proxy_atom = XInternAtom (x_dpy, FEEDBACK_PROXY_WIN, TRUE);

    /* Atom Exists */
    if (!proxy_atom)
      return FALSE;

    while (!proxy_prop_exist && i < num_screens)
      {
	GdkScreen *tmp_scr = gdk_display_get_screen (dpy, i);
	Window xroot = gdk_x11_drawable_get_xid (gdk_screen_get_root_window (tmp_scr));
	/* Proxy prop is set */
	if (XGetWindowProperty(x_dpy, xroot, proxy_atom, 0, 1,
			       False, XA_WINDOW, &actual_type, &actual_format,
			       &n_items_return, &bytes_after_return,
			       (unsigned char **)(&proxy_win)) == Success)
	  {
	    /* proxy prop previously existed */
	    proxy_prop_exist = TRUE;
	    /* keep root winid if needed */
	    *root = xroot; 
	    galf_verbose ("proxy_prop exist on root %ld\n", xroot);
	  }
	i++;
      }
    
    if  (!proxy_win || !proxy_prop_exist || n_items_return == 0)
	  return FALSE;
    
    galf_verbose ("proxy window declared is %ld\n", *proxy_win);
    
    /* set specific atom (avoid window ID reuse)*/
    gdk_error_trap_push ();
    
    result = XGetWindowProperty(x_dpy, *proxy_win, proxy_atom, 0, 1,
				False, XA_WINDOW, &actual_type, &actual_format,
				&n_items_return, &bytes_after_return,
				(unsigned char **)(&check_win));
    if (gdk_error_trap_pop () || result != Success)
      {
	galf_verbose ("proxy window doesn't exist\n");
	return FALSE;
      }
    if (!check_win)
      return FALSE;

    if (*check_win == *proxy_win)
      return *proxy_win;
    else
      return FALSE;
}

gboolean get_property (Display *dpy,
		       Window   win,
		       char   **data,
		       Atom     prop_name,
		       gboolean remove_prop)
{
  Atom actual_type;
  int actual_format;
  unsigned long n_items_return, bytes_after_return;
  long max_length; 
#define MAX_LENGTH 500

  max_length = MAX_LENGTH;

  if (XGetWindowProperty (dpy,
                          win,
                          prop_name,
                          0,
                          max_length,
                          False,
                          AnyPropertyType,
                          &actual_type,
                          &actual_format,
                          &n_items_return,
                          &bytes_after_return,
                          (unsigned char **) (data)) != Success)
    return False;
    
  if (bytes_after_return > 0 || remove_prop == True)
  { 
        if (*data)           
        {                  
                XFree(*data);
                *data = NULL;
        }

        if (bytes_after_return > 0)
                max_length = MAX_LENGTH + (bytes_after_return/4) + 1;
        else
                max_length = MAX_LENGTH;

        if (XGetWindowProperty (dpy,
                                win,
                                prop_name,
                                0,
                                max_length,
                                remove_prop,
                                AnyPropertyType,
                                &actual_type,
                                &actual_format,
                                &n_items_return,
                                &bytes_after_return,
                                (unsigned char **) (data)) != Success)
        return False;
  }
  if((actual_type == None) &&
     (actual_format == 0)  &&
     (bytes_after_return == 0))
      return False;
  return True;
}

gboolean get_feedback_request_details (Display *x_dpy, 
				       Window  win,
				       Atom request_atom, 
				       AppDetails *details)
{
  char *data = NULL;
  char *pch;
  
  if (!get_property (x_dpy, win, &data, request_atom, True))
    return FALSE;

  galf_verbose ("%s\n", data);

  /* details = g_new0 (AppDetails, 1); */

  pch = strtok (data, "#");
  if (pch)
    {
      details->app_name = g_strdup (pch);
      pch = strtok (NULL, "#");
    }
  else goto error;
  if (pch)
    {
      details->feedback_type = g_strdup (pch);
      pch = strtok (NULL, "#");
    } else goto error;
  if (pch)
    {
      details->timeout = atoi (pch);
      pch = strtok (NULL, "#");
    } else goto error;
  if (pch)
    {
      details->screen_num = atoi (pch);
      pch = strtok (NULL, "#");
    } else goto error;
  if (pch)
    {
      details->pid = atoi (pch);
      pch = strtok (NULL, "#");
    } else goto error;
  if (pch)
      details->command_line = g_strdup (pch);
      
  XFree (data);
  return TRUE;

error:
  XFree (data);
  return FALSE;
}

static Atom 
get_free_prop_atom (Display *dpy, Window prop_holder)
{
  gboolean prop_used = TRUE;
  int i = 0;
  char tmp_atom_name[100];
  Atom tmp_atom;
  char *prop_return = NULL;

  while (prop_used)
    {
      sprintf (tmp_atom_name, "%s%d", GALF_FEEDBACK_PROP_TEMPLATE, i);
      tmp_atom = XInternAtom (dpy, tmp_atom_name, FALSE);
      if (!get_property (dpy, prop_holder, &prop_return, tmp_atom, FALSE))
	  prop_used = FALSE;
      if (prop_return)
	XFree (prop_return);
      i++;
    }
  return tmp_atom;

}

gboolean
set_property (Display *dpy,
	      Window   dest,
	      char    *data,
	      Atom     prop_atom)
{
  XChangeProperty (dpy, dest, prop_atom, XA_STRING, 8,
		   PropModeReplace, (unsigned char *) data, strlen ((char *) data) + 1);
  galf_verbose ("Setting prop on window %ld\n", dest);
  XFlush (dpy);
  return TRUE;
}
	      
	      
gboolean 
set_feedback_request_details (Display *x_dpy,
			      Window   win,
			      AppDetails *details)
{
  gchar *details_str = g_new (char, strlen (details->app_name)+1+strlen (details->feedback_type)+1+strlen (details->command_line)+1+30);

  sprintf (details_str, "%s#%s#%u#%u#%u#%s", 
	   details->app_name, details->feedback_type,
	   details->timeout, details->screen_num, details->pid,
	   details->command_line);

  galf_verbose ("%s\n",details_str);

  set_property (x_dpy, win, details_str, get_free_prop_atom (x_dpy, win));
  return TRUE;
}

/*
 * set_desktop_launch_id
 * @launch_id: gchar pointer to point to a unique string identifying
 *             this feedback display instance 
 * Sets launch_id to a unique string consisting of machine name, display name, 
 * and pid of galf. Sets environmental variable DESKTOP_LAUNCH_ID to value of
 * launch_id
 * Returns TRUE if successful, else returns FALSE
 */
/* static gboolean 
set_desktop_launch_id(gchar *launch_id)
{
	static gchar *launch_env;
	gboolean success;
	gchar *machine_name=NULL, *display_name=NULL, hostname[BUFSIZ+1];
	if(gethostname(hostname, BUFSIZ)==0)
		machine_name=g_strdup(hostname);
	display_name=g_strdup(getenv("DISPLAY"));
	if (machine_name!=NULL && display_name!=NULL){
		g_snprintf(launch_id, BUFSIZ, "%s %s %d", machine_name, display_name, (int)getpid());
		launch_env=g_strdup_printf("DESKTOP_LAUNCH_ID=%s", launch_id); 	
		putenv(launch_env);
		success=TRUE;
	}else{
		success=FALSE;
	}
	g_free(machine_name);
	g_free(display_name);
	return success;
}*/



