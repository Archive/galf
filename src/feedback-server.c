/* 
 * copyright (C) 2001 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <glib.h>
#include <unistd.h>
#include <signal.h>
#define  WNCK_I_KNOW_THIS_IS_UNSTABLE
#include <libwnck/libwnck.h>
#include "feedback.h"
#include "hourglass.xpm"
#include "cursor.xbm"
#include "cursor_mask.xbm"
/* #include "plugin.h" */

GdkDisplay *dpy;
Display *x_dpy;
GdkScreen *default_screen;
GdkWindow* root_window;
Window xroot;
GdkCursor *cursor;
GdkPixbuf *icon;

Prefs prefs;

GSList  *internal_windows;
GSList  *processed_windows;
guint   occurences = 0;

gboolean reload;

static GdkFilterReturn
rootwin_event_monitor  (GdkXEvent *gdk_xevent,
                        GdkEvent *event,
                        gpointer data);

static void
stop_feedback (AppDetails *details);

GdkFilterReturn
filter_func (GdkXEvent  *gdkxevent,
             GdkEvent   *event,
             gpointer    data)
{
  XEvent *xevent = gdkxevent;
  if (xevent->type == PropertyNotify)
    {
      char *prop_name = XGetAtomName (x_dpy, xevent->xproperty.atom);
      /* galf_verbose ("property %s changed\n", prop_name); */
      if (strncmp (prop_name, 
		   GALF_FEEDBACK_PROP_TEMPLATE, 
		   strlen (GALF_FEEDBACK_PROP_TEMPLATE)) == 0)
	{
	  AppDetails *details = g_new0 (AppDetails, 1);
	  if (get_feedback_request_details (x_dpy, xevent->xproperty.window, 
					    xevent->xproperty.atom,
					    details))
	    {
	      galf_verbose ("AppDetails\n\tapp_name = %s\n\ttimeout = %d\n"
		       "\tfeedback_type = %s\n\t cmd_line = %s\n"
		       "\tscreen_num = %d\n",
		       details->app_name, details->timeout, 
		       details->feedback_type, details->command_line,
		       details->screen_num);
	      start_with_feedback (dpy, details);
	    }
	}
      XFree (prop_name);
    }
  return GDK_FILTER_CONTINUE;
}

/*
void setup_plugin ()
{
  char *tmp;
  tmp = g_strconcat ("/export/home/multihead/galf/src/", prefs.plugin, NULL);
  galf_verbose ("loading %s\n", tmp);
  plugin.module = g_module_open (tmp, 0);
  g_free (tmp);
  if (!plugin.module)
    {
      galf_verbose ("Error: plugin %s not found, trying the default one", tmp);
      plugin.module = g_module_open ("/export/home/multihead/galf/src/cursor", 0);
      if (!plugin.module)
	{
	  fprintf(stderr, "Error: Plugin cannot be loaded\n");
	  exit (1);
	}
      if (prefs.plugin_name)
	g_free (prefs.plugin_name);
      prefs.plugin_name = g_strdup ("cursor");
    }
  if(!g_module_symbol(plugin.module, "start_plugin", (gpointer*)&plugin.start_plugin))
    {
      fprintf(stderr, "Error: Plugin does not contain start_plugin function\n");
      exit (1);
    }
  if(!g_module_symbol(plugin.module, "stop_plugin", (gpointer*)&plugin.stop_plugin))
    {
      fprintf(stderr, "Error: Plugin does not contain stop_plugin function\n");
      exit (1);
    }
  if(!g_module_symbol(plugin.module, "close_plugin", (gpointer*)&plugin.close_plugin))
    {
      fprintf(stderr, "Error: Plugin does not contain stop_plugin function\n");
      exit (1);
    }
*/
int main(int argc, char** argv)
{
  Atom proxy_window_atom;
  GdkWindow *proxy_win;
  Window proxy;
  GError *error = NULL;
  GdkBitmap *bitmap;
  GdkBitmap *mask;
  GdkColor white = {0,0xFFFF,0xFFFF,0xFFFF};
  GdkColor black = {0,0x0000,0x0000,0x0000};
  
  gtk_init (&argc, &argv);

  dpy = gdk_display_get_default ();
  default_screen = gdk_display_get_default_screen (dpy);
  root_window = gdk_screen_get_root_window (default_screen);
  xroot = gdk_x11_drawable_get_xid (GDK_DRAWABLE (root_window));
  x_dpy = gdk_x11_display_get_xdisplay (dpy);

  proxy_window_atom = XInternAtom (x_dpy, FEEDBACK_PROXY_WIN, FALSE);
  
  /* sanity check */

  if (check_server_running (dpy, &xroot))
    {
      g_print (_("A feedback server is already running, exiting\n"));
      exit(0);
    }
  
  icon = gdk_pixbuf_new_from_xpm_data ((const char**)hourglass_xpm);

  /* create cursor */

  bitmap = gdk_bitmap_create_from_data (NULL, (const char *) cursor_bits,
					cursor_width, cursor_height);
    
  mask = gdk_bitmap_create_from_data (NULL, (const char *) cursor_mask_bits,
				      cursor_mask_width, cursor_mask_height);  

  cursor = gdk_cursor_new_from_pixmap (bitmap, mask,
				       &white, &black,
				       0, 0);
  /* Create the proxy window */
  
  proxy = XCreateSimpleWindow(x_dpy, xroot, 1, 1, 1, 1, 0, 0, 0);
  proxy_win = gdk_window_foreign_new (proxy);

  galf_verbose ("proxy window ID : %ld\n", proxy);
  
  XChangeProperty(x_dpy, xroot, proxy_window_atom, XA_WINDOW, 32, PropModeReplace,
		  (unsigned char *)&proxy, 1);
  XChangeProperty(x_dpy, proxy, proxy_window_atom, XA_WINDOW, 32, PropModeReplace,
		  (unsigned char *)&proxy, 1);

  /*collect prefs */

  prefs.warn_if_process_died = FALSE;
  
  /* load plugin */

 /* if (!g_module_supported ())
    {
      g_print ("Error: plugin not supported on this system\n");
      exit (1);
    }
  
  setup_plugin (); */
  
      
  /* start monitoring */
  XSelectInput (x_dpy, proxy, PropertyChangeMask);

  gdk_window_add_filter (proxy_win, filter_func, NULL);
  gtk_main();
  return 0;
}	

static void
set_cursor_all_screens (GdkDisplay *dpy, GdkCursor *cursor)
{
  int i, num_screens;
  
  num_screens = gdk_display_get_n_screens (dpy);
  
  for (i = 0; i < num_screens; i++)
    {
      GdkScreen *scr = gdk_display_get_screen (dpy, i);
      GdkWindow *root = gdk_screen_get_root_window (scr);
      gdk_window_set_cursor (root, cursor);
      /* galf_verbose ("Set cursor for root %d\n", root); */
    }
}

static gboolean manual_stop (GtkWidget *window,
			     GdkEvent  *event,
			     AppDetails *details)
{
  /* galf_verbose ("in toto event type %d\n", event->type); */
  if (event->type == GDK_UNMAP)
    {
      stop_feedback (details);
      return TRUE;
    }
  return FALSE;
}

static gboolean 
start_feedback (GdkDisplay *dpy, AppDetails *details)
{  
  Window root = None;
  Window child = None;
  int rootx, rooty;
  int winx = 0;
  int winy = 0;
  unsigned int xmask = 0;

  /* plugin placeholder */
  GtkWidget *top_level;
  GdkScreen *screen;
  gchar *title;
  
  occurences++;
  
  /* taskbar window */
  screen = gdk_display_get_screen (dpy, details->screen_num);
  title = g_strconcat ("(", details->app_name, ")", NULL);
  
  top_level = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (top_level), title);
  g_free (title);
  gtk_window_set_policy (GTK_WINDOW (top_level), FALSE, FALSE, TRUE);
  
  gtk_window_set_screen (GTK_WINDOW (top_level), screen);
  
  gtk_widget_realize (top_level);
  gdk_window_set_decorations (top_level->window, 0);
  
  g_signal_connect (top_level, "event", G_CALLBACK (manual_stop), details); 
  gtk_window_set_icon (GTK_WINDOW (top_level), icon);
  
  gtk_widget_show_all (top_level);
  details->feedback->fake_task_win = top_level;
  internal_windows = g_slist_append (internal_windows, top_level);

  /* busy cursor */

  gdk_error_trap_push ();
  
  XQueryPointer (x_dpy,
		 GDK_WINDOW_XID (gdk_screen_get_root_window (screen)),
		 &root, &child, &rootx, &rooty, &winx, &winy, &xmask);
  
  gdk_error_trap_pop ();
  if (child != None)
    {
      GdkWindow *gdk_child = gdk_window_foreign_new (child);

      /* galf_verbose ("got a window under the pointer %d\n", child); */
      
      gdk_window_set_cursor (gdk_child, cursor);
      details->feedback->pointer_window = child;
      g_object_unref (gdk_child);
    }
  else
    {
      details->feedback->pointer_window = None;
      /* galf_verbose ("no window under the pointer\n"); */
    }
  
  set_cursor_all_screens(dpy, cursor);
  return TRUE;
}

static void
stop_feedback (AppDetails *details)
{
  GdkScreen *screen = gdk_display_get_screen (gdk_display_get_default (),
					      details->screen_num);
  galf_verbose ("in stop_feedback %d\n", occurences);
  
    { /* plugin space */

      if (internal_windows)
	internal_windows = g_slist_remove (internal_windows, details->feedback->fake_task_win);

      if (GTK_IS_WINDOW (details->feedback->fake_task_win))
	gtk_widget_destroy (details->feedback->fake_task_win);

      galf_verbose ("destroy taskbar window. num occ %d\n", occurences);

      if (details->feedback->pointer_window != None)
	{
	  GdkWindow *gdk_child;
	  
	  gdk_error_trap_push ();
	  
	  gdk_child = gdk_window_foreign_new (details->feedback->pointer_window);
	  
	  if (gdk_child)
	    {
	      gdk_window_set_cursor (gdk_child,  gdk_cursor_new (GDK_LEFT_PTR));
	      g_object_unref (gdk_child);
	    }
	    
	  gdk_error_trap_pop ();
	  
	}
      if (occurences == 1)
	set_cursor_all_screens (dpy, gdk_cursor_new (GDK_LEFT_PTR));
    }

  if (kill (details->pid, 0) == -1)
    {
      if (prefs.warn_if_process_died)
	{
	  GtkWidget *dialog = gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_WARNING,
						      GTK_BUTTONS_CLOSE,
						      _("the following application :\n (%s)\n seems to either:\nspawned another process and exited or\ndied unexpectedly or terminated"),
						      details->command_line);
	  gtk_window_set_screen (GTK_WINDOW (dialog), screen);
	  g_signal_connect (dialog, "response", G_CALLBACK (gtk_widget_destroy), NULL);
	  gtk_widget_show (dialog);
	}
    }

  details->feedback->stopped = TRUE;
  occurences--;
  if (occurences == 0)
    {
      g_slist_free (processed_windows);
      processed_windows = NULL;
    }
  g_signal_handler_disconnect (wnck_screen_get(details->screen_num), details->window_opened_handler);
}

gboolean is_win_internal (Window window)
{
  GSList *tmp = internal_windows;

  while (tmp)
    {
      GtkWidget *top_level = tmp->data;
      XID internal_win = gdk_x11_drawable_get_xid (GDK_DRAWABLE (top_level->window));
      if (window == internal_win)
	{
	  galf_verbose ("win is internal\n");
	  return TRUE;
	}
      tmp = tmp->next;
    }
  return FALSE;
}

static gboolean 
stop_feedback_timeout (AppDetails *details)
{
  galf_verbose ("in stop_feedback_timeout (stopped = %d)\n",details->feedback->stopped);
  if (!details->feedback->stopped)
      stop_feedback (details);
  
  free_app_details (details);
  return FALSE;
}

static void
window_opened (WnckScreen   *screen,
               WnckWindow   *window,
	       AppDetails   *details)
{
  GSList* element = NULL;
  
  galf_verbose("Window Opened %s\n", wnck_window_get_name (window));
  
  if (processed_windows)
    element = g_slist_find (processed_windows, window);
    
  if (!element && 
      !is_win_internal (wnck_window_get_xid (window)))
    {
      processed_windows = g_slist_append (processed_windows, window);
      stop_feedback (details);
    }
}



gboolean start_with_feedback (GdkDisplay *dpy, AppDetails *details)
{
  GError *error = NULL;
  WnckScreen *wnck_screen;
  
  Display *x_dpy = gdk_x11_display_get_xdisplay (dpy);
  GdkScreen *screen = gdk_display_get_screen (dpy, details->screen_num);
  GdkWindow *root = gdk_screen_get_root_window (screen);
  Window xroot = gdk_x11_drawable_get_xid (root);
  
  
  details->feedback = g_new0 (Feedback, 1);
  
  start_feedback (dpy, details);

  XFlush (x_dpy);
  
  wnck_screen = wnck_screen_get (details->screen_num);
  wnck_screen_force_update (wnck_screen);

  details->window_opened_handler = g_signal_connect (G_OBJECT (wnck_screen), "window_opened", G_CALLBACK (window_opened), details);

  g_timeout_add (details->timeout * 1000, (GSourceFunc)stop_feedback_timeout, details); 

  return TRUE;
}


