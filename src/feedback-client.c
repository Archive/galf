/* 
 * copyright (C) 2001 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <popt.h>
#include <stdlib.h>
#include "feedback.h"

char *title;
int timeout;
char *feedback_type;
char *command_line;
gboolean feedback_only;

const struct poptOption popt_options[] = {
	{ "title", 't', POPT_ARG_STRING, &title, 1,
	  "Specify a program title to use in feedback display\n", "STRING"},
	{ "command-line", 'c', POPT_ARG_STRING, &command_line, 2,
	"Specify command line to spawn\n", "STRING"},
	{ "timeout",'o',POPT_ARG_INT, &timeout, 3,
	  "Specify the who long the feedback will be displayed if the app does not reply\n", "INT"},  
/*	{ "feedback-type", 'f', POPT_ARG_STRING, &feedback_type, 4,
	"Specify a specific type of feedback\n", "STRING"},*/
	{ "feedback-only", 'f', POPT_ARG_NONE, &feedback_only, 4,
	  "Only display feedback until a window appears\n", ""},
	POPT_AUTOHELP
       	{NULL, '\0', 0, NULL, 0, NULL, NULL}
};

gboolean
remove_args (const char *long_arg, char short_arg, 
	     int argc, char **argv, gboolean arg)
{
  int i;
  char buf_long[256];
  char buf_short[3];
  gboolean result = FALSE;
  
  sprintf (buf_long, "--%s", long_arg);
  sprintf (buf_short, "-%c", short_arg);
  
  for (i=0; i <= argc; i++)
    {
      if (argv[i])
	{
	  if (strcmp (argv[i], buf_short) == 0 
	      || strcmp (argv[i], buf_long) == 0)
	    {
	      argv[i] = NULL;
	      if (arg)
		argv[i+1] = NULL;
	      result = TRUE;
	      return result; /*return at 1st occ for now*/
	    }
	}
    }
  return result;
}


	 
int main (int argc, char **argv)
{
  char** argv_copy, **argv_app;
  int argc_copy, argc_app = 0;
  int i = 0, result = 0, j = 0;
  AppDetails *feedback_details;
  Window proxy_window = 0, xroot;
  GdkScreen *screen;
  Display *x_dpy;
  GError *error = NULL;
  gboolean exec_only = FALSE;

  /* pctx = poptGetContext(NULL, argc, (const char **)argv, popt_options, 0); */

  poptDupArgv (argc, (const char **)argv, &argc_copy, (const char ***)&argv_copy);
  
  /* parse the arguments, remove galf specific commands*/
  
/*  while ((result = poptGetNextOpt(pctx)) > 0)
    {
      gboolean arg = TRUE;

      if (result >= 4)
	arg = FALSE;
      
      remove_args (popt_options[result-1].longName,
		   popt_options[result-1].shortName,
		   argc_copy, argv_copy, arg);
    }
*/
  /* create contigu argv */
  
  for (i=0;i<argc_copy;i++)
      if (argv_copy[i] != NULL)
	  argc_app++;

  argv_app = g_new (char *, argc_app + 1);
  for (i = 0; i < argc_copy; i++)
    if (argv_copy [i])
      argv_app [j++] = g_strdup (argv_copy [i]);
  g_assert (j == argc_app);
  argv_app [j] = NULL;
  
  for (i=0;i<argc_app;i++)
    if (argv_app[i] != NULL)
      galf_verbose ("argv_app[%i] = %s\n", i, argv_app[i]);

  gtk_init (&argc_app, &argv_app); 

  x_dpy = gdk_x11_display_get_xdisplay(gdk_display_get_default ());

  if ((proxy_window = check_server_running (gdk_display_get_default (), &xroot)) == 0)
    {
      GError *error = NULL;
      /* Start the server */
      galf_verbose ("Starting a server\n");
      g_spawn_command_line_async ("galf-server", &error);
      if (error)
	{
	  fprintf (stderr, _("couldn't start the feedback server\n"));
	  exec_only = TRUE;
	}
    }

  feedback_details = g_new (AppDetails, 1);
  
  if (title)
    feedback_details->app_name = title;
  else if (command_line)
    feedback_details->app_name = command_line;
  else
    {
      if (argv_app[1])
	feedback_details->app_name = g_strdup (argv_app[1]);
      else
	feedback_details->app_name = g_strdup ("none");
    }

  if (timeout != 0)
    feedback_details->timeout = timeout;
  else
    feedback_details->timeout = DEFAULT_TIMEOUT;

  if (feedback_type)
    feedback_details->feedback_type = feedback_type;
  else
    feedback_details->feedback_type = DEFAULT_FEEDBACK_TYPE;

  if (command_line)
    feedback_details->command_line = command_line;
  else
    {
      /*concat the remaining argv[] in a string */
      int len = 1;

      if (argc_app == 1)
	  exit (1);
      
      for (i=1;i<argc_app;i++)
	len += strlen (argv_app[i]) + 1;
      
      feedback_details->command_line = g_new0 (char, len);
      
      for (i=1;i<argc_app;i++)
	{
	  strcat (feedback_details->command_line, argv_app[i]);
	  strcat (feedback_details->command_line, " ");
	}
    }
  screen = gdk_screen_get_default ();
  feedback_details->screen_num = gdk_screen_get_number (screen);


  g_strfreev (argv_app);

  g_shell_parse_argv (feedback_details->command_line, NULL, &argv_app, 
		      &error);
  
  if (!g_spawn_async (NULL,
		      argv_app,
		      NULL,
		      G_SPAWN_SEARCH_PATH,
		      NULL,
		      NULL,
		      &feedback_details->pid,
		      &error))
      {
	GtkWidget *dialog = gtk_message_dialog_new (NULL, 0, GTK_MESSAGE_WARNING,
						    GTK_BUTTONS_CLOSE,
						    _("Error while trying to start "
						    "the following application :\n (%s)"),
						    feedback_details->command_line);
	gtk_window_set_screen (GTK_WINDOW (dialog), screen);
	g_signal_connect (dialog, "response", G_CALLBACK (exit), NULL);
	gtk_widget_show (dialog);
	gtk_main ();
      }

  g_strfreev (argv_app);
  
  gdk_error_trap_push ();

  if (!exec_only)
    set_feedback_request_details(x_dpy, proxy_window,
				 feedback_details);

  gdk_error_trap_pop ();
  
  galf_verbose ("AppDetails\n\tapp_name = %s\n\ttimeout = %d\n"
		"\tfeedback_type = %s\n\tscreen_num = %d\n"
		"\tpid = %d\n\t cmd_line = %s%\n",
		feedback_details->app_name, 
		feedback_details->timeout, 
		feedback_details->feedback_type,
		feedback_details->screen_num,
		feedback_details->pid,
		feedback_details->command_line);
  return 0;
}

