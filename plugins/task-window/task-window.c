/*
 * copyright (C) 2001 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gdk/gdkprivate.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <gmodule.h>
#include "../../src/plugin.h"
#include "hourglass.xpm"
#include <gnome.h>


/*
 * the code for the functions create_invisible_window and set_icon
 * were taken from xalf (X application launch feedback):  
 * Copyright Peter Astrand <altic@lysator.liu.se> 2000. GPLV2.
 */

static void
create_invisible_window(gchar *title);
static void
set_icon(GdkWindow *window);

gint
init_plugin(PluginInfo *pi)
{
	pi->plugin_name=("Tasklist Window");
	pi->desc=("Displays a window with hourglass in the Tasklist");
        pi->author=("Mary Dwyer");
	create_invisible_window(pi->launched_app_name);
	return PLUGIN_OK;
}

static void
create_invisible_window(gchar *title)
{
	GtkWidget *window;
	gchar *tasktitle;

	tasktitle = g_strconcat ("(", title, ")", NULL);
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW(window), tasktitle);
	gtk_window_set_policy (GTK_WINDOW(window), FALSE, FALSE, TRUE);
	gtk_window_set_wmclass (GTK_WINDOW(window), "galf", "galf");
	gtk_widget_realize (window);
	gdk_window_set_decorations (window->window, 0);
	/* Set a hourglass icon for the indicator via KWM_WIN_ICON */
	set_icon (window->window);

	/* Show window */
	gtk_widget_show (window);
}

static void
set_icon (GdkWindow *window)
{
	static GdkPixmap *w_minipixmap = NULL;
	static GdkBitmap *w_minimask = NULL;
	GdkAtom icon_atom;
	glong data[2];

	w_minipixmap =
		gdk_pixmap_create_from_xpm_d (window,
				&w_minimask,
				NULL,
                                hourglass_xpm);

	data[0] = ((GdkPixmapPrivate *)w_minipixmap)->xwindow;
	data[1] = ((GdkPixmapPrivate *)w_minimask)->xwindow;

	icon_atom = gdk_atom_intern ("KWM_WIN_ICON", FALSE);
	gdk_property_change (window, icon_atom, icon_atom,
			32, GDK_PROP_MODE_REPLACE,
	 		(guchar *)data, 2);
}

